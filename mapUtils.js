import * as Util from './utils.js';
import {ELEVATION_DATA_FOLDER_PATH, INIT_EVLEVATION_VALUE} from './const.js';

// array of canvas in DOM
function getCanvas() {
    return document.getElementsByClassName('canvas-elevation');
};

// array of bbox covered elevation files
function getElevationFileNames(map) { //Leaflet map, 
    console.log(map.getBounds());
    let lat1 = map.getBounds().getNorth();
    let lon1 = map.getBounds().getEast();
    let lat2 = map.getBounds().getSouth();
    let lon2 = map.getBounds().getWest();

    let lon1_letter = (lon1>=0) ? 'E' : 'W'
    let lat1_letter = (lat1>=0) ? 'N' : 'S'
    let lon2_letter = (lon2>=0) ? 'E' : 'W'  
    let lat2_letter = (lat2>=0) ? 'N' : 'S'
    
    // the same hemisphere
    if (lon1_letter != lon2_letter || lat1_letter != lat2_letter){
        print("err not the same hemisphere")
        return
    }

    let lon1_tilenum = (Math.floor(lon1))
    let lat1_tilenum = (Math.floor(lat1))
    let lon2_tilenum = (Math.floor(lon2))
    let lat2_tilenum = (Math.floor(lat2))

    lat1 = lat1_tilenum;
    lat2 = lat2_tilenum;

    if(lat1_tilenum < lat2_tilenum){
        lat1 = lat2_tilenum;
        lat2 = lat1_tilenum;
    }

    lon1 = lon1_tilenum;
    lon2 = lon2_tilenum;

    if(lon1_tilenum < lon2_tilenum){
        lon1 = lon2_tilenum;
        lon2 = lon1_tilenum;
    }
    
    let source_files = [];

    for(let lat=lat2; lat<=lat1; lat++){
        for(let lon=lon2; lon<=lon1; lon++){
            let latVal = Util.pad(parseInt(Math.abs(lat)), 2);
            let lonVal = Util.pad(parseInt(Math.abs(lon)), 3);
            let filename = `${lat1_letter}${latVal}${lon1_letter}${lonVal}.hgt.png`;
            source_files.push({
                id: filename,
                lat: parseInt(lat),
                lon: parseInt(lon),
                path: `${filename.slice(0,2)}/${filename}`,
            });
        }
    }
    return source_files;             
};

export const updateCanvas = async(map, elevation=INIT_EVLEVATION_VALUE) => { //HTML document
    let currentCanvas = getCanvas();
    let newElements = getElevationFileNames(map);

    let elToRemove = Util.arrayDiff(currentCanvas, newElements);
    let elToAdd = Util.arrayDiff(newElements, currentCanvas);

    // remove elements
    for(let i=0; i<elToRemove.length; i++){
        var elem = document.getElementById(elToRemove[i].id);
         elem.parentElement.removeChild(elem);
    }

    // add elements
    for(let i=0; i<elToAdd.length; i++){
        let file_path = ELEVATION_DATA_FOLDER_PATH + '/' + elToAdd[i].path;
        Util.imageExists(file_path, function(exists){
            if(exists){
                L.imageOverlay.canvas(file_path, [
                                                    [elToAdd[i].lat < 0 ? elToAdd[i].lat + 0.999999 : elToAdd[i].lat,
                                                     elToAdd[i].lon < 0 ? elToAdd[i].lon + 0.999999 : elToAdd[i].lon],
                                                    [elToAdd[i].lat < 0 ? elToAdd[i].lat : elToAdd[i].lat + 0.999999,
                                                     elToAdd[i].lon < 0 ? elToAdd[i].lon : elToAdd[i].lon + 0.999999]
                                                ],
                                                {
                                                    init_elevation: elevation,
                                                    id:				elToAdd[i].id,
                                                }).addTo(map);
            }else{
                console.log(`Warning file "${file_path}" not exists.`);
            }
        });
    }
};

export const loadElevationData = async(elevation) =>{
    let tiles = document.getElementsByClassName('canvas-elevation');
    for(let i=0; i<tiles.length; i++){
        var canvas = tiles[i];
        var context = canvas.getContext("2d");
        var base_image = new Image();
        base_image.src = canvas.src;
        context.drawImage(base_image, 0, 0, canvas.width, canvas.height);
        await processElevation(canvas, elevation);      
    }
}

export const setElevation = async(elevation) =>{
    await loadElevationData(elevation);
}

export const processElevation = async(canvas, elevation) =>{
    var context = canvas.getContext("2d");
    var imgData = context.getImageData(0,0,canvas.width,canvas.height);
    var data = imgData.data;

    for(let j=0; j<data.length; j+=4) {
        let height = -10000 + ((data[j] * 256 * 256 + data[j+1] * 256 + data[j+2]) * 0.1);
        if (height <= elevation && data[j+3] != 0) {
            data[j] = 115;
            data[j+1] = 182;
            data[j+2] = 230;
            data[j+3] = 175;
        }else{
            data[j+3] = 0;
        }
    }
    context.putImageData(imgData, 0, 0);
};