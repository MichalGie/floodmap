import * as MapUtil from './mapUtils.js';

L.ImageOverlay.Canvas = L.ImageOverlay.extend({

    _create: function(tagName, id, className, container) {
        var el = document.createElement(tagName);
        el.setAttribute("id", id);
        el.className = className || '';
    
        if (container) {
            container.appendChild(el);
        }
        return el;
    },

    _initImage: function () {
        var elevation = this.options.init_elevation;
        var wasElementSupplied = this._url.tagName === 'CANVAS';
        
        var topLeft = this._map.latLngToLayerPoint(this._bounds.getNorthWest()),
        size = this._map.latLngToLayerPoint(this._bounds.getSouthEast())._subtract(topLeft);
        
        var image = this._image = this._create('canvas', this.options.id, 'leaflet-image-layer');
        image.width  = size.x;
        image.height = size.y;
           
        var base_image = new Image();
        base_image.src = this._url;
        
        base_image.onload = function(){
            image.getContext('2d').drawImage(base_image, 0, 0, size.x, size.y);
            MapUtil.processElevation(image, elevation);
        }

        L.DomUtil.addClass(image, 'leaflet-image-layer canvas-elevation');
        if (this._zoomAnimated) { L.DomUtil.addClass(image, 'leaflet-zoom-animated'); }
        if (this.options.className) { L.DomUtil.addClass(image, this.options.className); }

        this._image.onselectstart = L.Util.falseFn;
        this._image.onmousemove = L.Util.falseFn;

        // @event load: Event
        // Fired when the ImageOverlay layer has loaded its image
        this._image.onload = L.Util.bind(this.fire, this, 'load');
        this._image.onerror = L.Util.bind(this._overlayOnError, this, 'error');

        if (this.options.crossOrigin || this.options.crossOrigin === '') {
            this._image.crossOrigin = this.options.crossOrigin === true ? '' : this.options.crossOrigin;
        }

        if (this.options.zIndex) {
            this._updateZIndex();
        }

        if (wasElementSupplied) {
            this._url = this._image.src;
            return;
        }

        this._image.src = this._url;
        this._image.alt = this.options.alt;

        L.Util.extend(this._image, {
            galleryimg: 'no',
            onselectstart: L.Util.falseFn,
            onmousemove: L.Util.falseFn,
        });
    },
});


L.imageOverlay.canvas = function (url, bounds, options) {
    return new L.ImageOverlay.Canvas(url, bounds, options);
};