export function imageExists(url, callback) {
    var img = new Image();
    img.onload = function() { callback(true); };
    img.onerror = function() { callback(false); };
    img.src = url;
}

// ids of elements to add/remove from DOM
export function arrayDiff(array1, array2) {
    if(!array1){
        return [];
    }

    if(!array2){
        return array1;
    }
    
    let arrayDiff = [];
    for(let i=0; i<array1.length; i++) {
        let el = array1[i];
        let diff = true;

        for(let j=0; j<array2.length; j++) {
            if(el.id == array2[j].id){
                diff = false;
                break;
            }
        }
        if(diff) {
            arrayDiff.push(el);
        }
    }
    return arrayDiff;
}

// format number with leading zeros
export function pad(num, size) {
    var s = num+"";
    while (s.length < size) s = "0" + s;
    return s;
}