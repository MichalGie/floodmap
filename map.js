import * as MapUtil from './mapUtils.js';
import {INIT_EVLEVATION_VALUE} from './const.js'

var map = L.map('map').setView([54.823476,18.310730], 9);

L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
    maxZoom: 13,
    id: 'mapbox.streets',
}).addTo(map);


var pipsSlider = document.getElementById('slider-pips');

noUiSlider.create(pipsSlider, {
    range: {
        min: 0,
        max: 200
    },
    connect: [true, false],
    animate: true,
    animationDuration: 500,
    start: [INIT_EVLEVATION_VALUE],
    pips: {mode: 'count', values: 5}
});

var pips = pipsSlider.querySelectorAll('.noUi-value');

function clickOnPip() {
    var value = Number(this.getAttribute('data-value'));
    pipsSlider.noUiSlider.set(value);		
}

for (var i = 0; i < pips.length; i++) {

    // For this example. Do this in CSS!
    pips[i].style.cursor = 'pointer';
    pips[i].addEventListener('click', clickOnPip);
}

pipsSlider.noUiSlider.on('set.one', ()=>{
    var elevation_value = parseInt(pipsSlider.noUiSlider.get());
    var output = document.getElementById('output');
    output.innerText = elevation_value;
    MapUtil.setElevation(elevation_value);
});

window.addEventListener('load', ()=>{
    var elevation_value = parseInt(pipsSlider.noUiSlider.get());
    var output = document.getElementById('output');
    output.innerText = elevation_value;
});


document.getElementById('startSimulation').addEventListener('click', ()=>{
    toggleSimulation();
});

map.addEventListener('moveend', ()=>{
    var elevation_value = parseInt(pipsSlider.noUiSlider.get());
    MapUtil.updateCanvas(map, elevation_value);
});

map.whenReady(()=>{
    MapUtil.updateCanvas(map);
});

function toggleSimulation(){
    var value = parseInt(pipsSlider.noUiSlider.get());   
    
    var interval = setInterval(function() {
        value = value + 10;
        pipsSlider.noUiSlider.set(value);		
        
        if(value + 10 >= parseInt(pipsSlider.noUiSlider.options.range.max)){
            pipsSlider.noUiSlider.set(parseInt(pipsSlider.noUiSlider.options.range.max));
            clearInterval(interval);
        }

        }, 2000);
    }