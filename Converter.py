import os
import math
import struct
from pathlib import Path
from PIL import Image
from pprint import pprint

HGT_X_RESOLUTION = 1201
HGT_Y_RESOLUTION = 1201
DATA_INPUT_FOLDER_PATH = 'data/hgt/'
DATA_OUTPUT_FOLDER_PATH = 'data/terrain-rgb/'

DIR_PATH = os.path.dirname(os.path.abspath(__file__))
DATA_INPUT_FOLDER_ABS_PATH = os.path.join(DIR_PATH, DATA_INPUT_FOLDER_PATH)
DATA_OUTPUT_FOLDER_ABS_PATH = os.path.join(DIR_PATH, DATA_OUTPUT_FOLDER_PATH)


class SRTMReader:

    def _require_files(self, lonE, latN, lonW, latS):
        #filenames of required hgt source files
        source_files = []

        #NE
        lon1 = lonE
        lat1 = latN

        #sw
        lon2 = lonW
        lat2 = latS

        lon1_letter = 'E' if (lon1>=0) else 'W'  
        lat1_letter = 'N' if (lat1>=0) else 'S'
        lon2_letter = 'E' if (lon2>=0) else 'W'  
        lat2_letter = 'N' if (lat2>=0) else 'S'
        
        #the same hemisphere
        if not (lon1_letter == lon2_letter and lat1_letter == lat2_letter):
            print("err not the same hemisphere")
            return
   
        lon1_tilenum = abs(math.floor(lon1))
        lat1_tilenum = abs(math.floor(lat1))
        lon2_tilenum = abs(math.floor(lon2))
        lat2_tilenum = abs(math.floor(lat2))

        if(lat1_tilenum >= lat2_tilenum):
            lat1 = lat1_tilenum
            lat2 = lat2_tilenum
        else:
            lat1 = lat2_tilenum
            lat2 = lat1_tilenum

        if(lon1_tilenum >= lon2_tilenum):
            lon1 = lon1_tilenum
            lon2 = lon2_tilenum
        else:
            lon1 = lon2_tilenum
            lon2 = lon1_tilenum

        for lat in list(range(lat2, lat1 + 1)):
            for lon in list(range(lon2, lon1 + 1)):
                filename = "{0}{1}{2}{3}{4}".format(
                    lat1_letter,
                    str("{:02d}".format(int(lat))),
                    lon1_letter,
                    str("{:03d}".format(int(lon))),
                    ".hgt")

                source_files.append(os.path.join(filename[0:2], filename))
        return source_files
        
    def _validate_files(self, files):
        err = False
        valid_files = []
        skip_files = []
        skip_file_path = os.path.join(DATA_INPUT_FOLDER_ABS_PATH, 'skip_files')
        if os.path.isfile(skip_file_path):
            with open(skip_file_path) as f:
                for line in f:
                    skip_files.append(line.strip())
        else:
            print("[Warning] Not found skip_file")

        for fname in files:
            if fname in skip_files:
                continue
            else:
                file_path = os.path.join(DATA_INPUT_FOLDER_ABS_PATH, fname)

                if not os.path.isfile(file_path):
                    print("[Err] No such file or directory: {}".format(file_path))
                    err = True
                valid_files.append(fname)
        return valid_files, not err

    def _convert_data_to_terrain_rgb(self, files):
        # height = -10000 + ((R * 256 * 256 + G * 256 + B) * 0.1)
        file_number = 0     
        for fname in files:
            file_number = file_number + 1;
            fn = fname.split('/')[1]
            filename = "{}.png".format(fn)
            output_file_dir_path = os.path.join(DATA_OUTPUT_FOLDER_ABS_PATH, filename[0:2])
            output_file_path = os.path.join(output_file_dir_path, filename)
            
            if not(os.path.isdir(output_file_dir_path)):
                os.makedirs(output_file_dir_path)

            if(os.path.isfile(output_file_path)):
                print('{}/{} file `{}` arleady exists'.format(file_number, len(files), filename))
                continue

            im = Image.new("RGB", (HGT_X_RESOLUTION, HGT_Y_RESOLUTION))
            pix = im.load()   
            x = 0
            y = 0
            input_file_path = os.path.join(DATA_INPUT_FOLDER_ABS_PATH, fname)
            with open(input_file_path, "rb") as f:
                byte = f.read(2)
                while byte:
                    
                    height = struct.unpack('>H', byte)[0]
                   
                    if height>32767:
                        height -= 65536
                    if height == -32768:
                        pix[x,y] = (0,0,0)
                    else:
                        val = (height + 10000) / 0.1
                        r = (val / (256 * 256)) % 256
                        g = (val / 256) % 256
                        b = val % 256
                        pix[x,y] = (int(r),int(g),int(b))

                    x = x + 1
                    if x % HGT_X_RESOLUTION == 0:
                        x = 0
                        y = y + 1

                    byte = f.read(2)
            im.save(output_file_path, "PNG")
            print('{}/{} converted `{}` file'.format(file_number, len(files), filename))
        return

    def bbox_hgt_to_terrain_rgb(self, lonE, latN, lonW, latS):
        if not (-90 <= latN <= 90 and -90 <= latS <= 90 and -180 <= lonE <= 180 and -180 <= lonW <= 180):
            print("[Err] Invalid coordinates given.")
            return
        
        source_files = self._require_files(lonE, latN, lonW, latS)
        
        source_files, err = self._validate_files(source_files)
        if not err:
            print("[Err] Please download missing files from https://dds.cr.usgs.gov/srtm/version2_1/SRTM3/")
            return
        
        self._convert_data_to_terrain_rgb(source_files)

    def all_hgt_to_terrain_rgb(self):
        source_files = []
        for filename in Path(DATA_INPUT_FOLDER_ABS_PATH).glob('**/*.hgt'):
            source_files.append(os.path.relpath(filename,DATA_INPUT_FOLDER_ABS_PATH))
       
        self._convert_data_to_terrain_rgb(source_files)

srtm = SRTMReader()

# srtm.bbox_hgt_to_terrain_rgb(-75.42938232421875, 41.2509675141624, -71.86157226562501, 40.19146303804063)

srtm.all_hgt_to_terrain_rgb()